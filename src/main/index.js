import React, { useEffect } from 'react';
import { createRoot } from 'react-dom/client';
import HOC from '@com/HOC';
import { getRealUrl } from 'utils/tool';
import sty from './main.scss';

function Main(props) {
  useEffect(() => {
    console.log(performance);
  }, []);

  return (
    <div className={sty.container}>
      首页-
      <p onClick={() => getRealUrl('page')}>跳转第二页aj as f</p>
    </div>
  );
}

const Page = HOC(Main);
const root = createRoot(document.getElementById('root'));

root.render(<Page />);
