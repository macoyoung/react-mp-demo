const HtmlWebpackPlugin = require('html-webpack-plugin');

// 页面，只需增加页面即可
const page = [
  { name: 'main', title: '主页' },
  { name: 'page', title: 'test-page' }
];

// 生成入口文件及html模板
const entries = {};

const htmlTemPlugin = page.map(item => {
  const { name, title } = item;

  entries[name] = `./src/${name}/index.js`;

  return new HtmlWebpackPlugin({
    title,
    hash: true,
    minify: true,
    filename: `${name}.html`,
    template: './html/index.html',
    chunks: ['mainfest', 'vendor', name]
  });
});

module.exports = { entries, htmlTemPlugin };
