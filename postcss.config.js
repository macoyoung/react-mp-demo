module.exports = () => {
  return {
    plugins: [
      'postcss-preset-env',
      ['postcss-px-to-viewport', {
        viewportWidth: 750,
        viewportHeight: 1134,
        landscapeWidth: 1134,
        selectorBlackList: [/^body$/],
        exclude: [/node_modules/]
      }]
    ]
  }
}