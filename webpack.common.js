const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { entries, htmlTemPlugin } = require('./html/pages');

module.exports = {
  entry: entries,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)$/,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 4096
          }
        },
        generator: {
          filename: 'images/[name].[hash:8][ext]'
        }
      }
    ]
  },

  // 设置别名
  resolve: {
    alias: {
      utils: path.resolve(__dirname, 'utils'),
      '@com': path.resolve(__dirname, 'components')
    },
    extensions: ['.js', '.json', '.jsx']
  },

  plugins: [
    ...htmlTemPlugin,
    new CleanWebpackPlugin({ path: path.resolve(__dirname, 'dist') })
  ]
};
